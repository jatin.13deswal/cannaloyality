import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Routes, Link } from 'react-router-dom';
import Products from './Pages/ProductsExplorer/ProductsExplorer.jsx';

function App() {
  return (
   <Router>

   <Routes>
   <Route path="/" element={<div>Home Page</div>} />
    <Route path="/explore-products" element={<Products />} />
   </Routes>
   </Router>
  );
}

export default App;
