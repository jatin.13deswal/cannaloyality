// SearchNavBar.js
import React from 'react';
import './SearchNavBar.css';

const SearchNavBar = () => {
  return (
    <header className="search-nav-bar">
      <h1 className="logo">CannaLoyal</h1>
      <div className="search-container">
        <input type="text" placeholder="Search products" className="search-input" />
        <button className="search-button">🔍</button>
      </div>
      <nav className="navigation">
        <a href="#" className="nav-link">Browse</a>
        <a href="#" className="nav-link">Product</a>
        <a href="#" className="nav-link">Product</a> {/* Duplicate 'Product' is used since it's shown in the image, but this should probably be different. */}
        <button className="cart-button">Show Cart</button>
      </nav>
    </header>
  );
};

export default SearchNavBar;
